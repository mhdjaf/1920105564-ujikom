<nav class="navbar navbar-expand-md navbar-dark" style="background: #D9534F; border-radius: 10px">

  <div class="container">

    <a href="#" class="navbar-brand">
      <img src="/img/logolagi.png" alt="" class="rounded-circle" style="width: 50px; margin-right: 4px">
      <strong>
        SMK Al Falah
      </strong>
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse flex justify-content-end navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item"><a href="#" class="nav-link text-white mr-2"><i class="fas fa-home"></i> Home</a></li>
      </ul>
      <ul class="navbar-nav">
        <li class="nav-item"><a href="#" class="nav-link text-white mr-2"><i class="far fa-address-card"></i> About</a></li>
      </ul>
      <ul class="navbar-nav">
        <li class="nav-item"><a href="#" class="nav-link text-white mr-2"><i class="fas fa-phone-alt"></i> Contact</a></li>
      </ul>
      <div class="dropdown">
        <a class="btn" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
          @if (Auth()->User()->level != 'admin')
            <img src="\foto_users\{{ Auth()->User()->foto_users }}" class="rounded-circle" alt="" width="35px" height="35px">
          @else
            <i class="fas fa-user-circle text-white fa-2x">
            </i>
          @endif
          
        </a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <li><a class="dropdown-item" href="/profile"><i class="fas fa-user-circle"></i> Profile</a></li>
          <li>
            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link text-white mr-2"><i class="fas fa-door-open"></i> Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
          </li>
        </ul>
      </div>
    </div>

  </div>

</nav>