@extends('layouts.siswa.dashboard')

@section('body')
<nav class="my-3">
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#table-siswa" role="tab" aria-controls="table-siswa" aria-selected="true">Tabel Siswa</a>
        <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Tabel Pembimbing</a>
        <a class="nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Tabel Perusahaan</a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    {{-- Tabel Siswa --}}
    <div class="tab-pane fade show active" id="table-siswa" role="tabpanel" aria-labelledby="nav-home-tab">
        <form method="GET" action="/table/siswa/cari">
            <label for="">Cari Siswa : </label>
            <div class="row">
                <div class="col-lg-5">
                    <div class="input-group mb-3">
                        <input type="text"  name="cari" class="form-control" value="{{ old('cari') }}"  placeholder="Tulis Nama/NIS" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <button class="btn btn-outline-primary ml-2"type="submit" value="cari" id="button-addon2"><i class="bi bi-search"></i></button>
                    </div>
                    <a href="/table/siswa" class="btn btn-primary mb-3 mt-3"><span class="bi bi-info"> Detail</span></a>
                </div>
            </div>
        </form>
        <table class="table table-responsive-sm">
            <thead class="thead text-white" style="background-color: #007bff">
                <tr>
                    <th scope="col">NIS</th>
                    <th scope="col">Nama Lengkap</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Jurusan</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($siswa as $siswa)
                  <tr>
                        <td>{{ $siswa->nis }}</td>
                        <td>{{ $siswa->name }}</td>
                        <td>{{ $siswa->kelas }}</td>
                        <td>{{ $siswa->jurusan->jurusan }}</td>
                  </tr>
              @endforeach
            </tbody>
          </table>
    </div>

    {{-- Tabel Pembimbing --}}
    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        <a href="/table/pembimbing" class="btn btn-primary mb-2"><span class="bi bi-info"> Detail</span></a>
        <table class="table table-responsive-sm">
            <thead class="thead text-white" style="background-color: #007bff">
                <tr>
                    <th scope="col">NIP</th>
                    <th scope="col">Nama Lengkap</th>
                    <th scope="col">Kejuruan</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($pembimbing as $pembimbing)
                  <tr>
                        <td>{{ $pembimbing->nis }}</td>
                        <td>{{ $pembimbing->name }}</td>
                        <td>{{ $pembimbing->jurusan->jurusan }}</td>
                  </tr>
              @endforeach
            </tbody>
          </table>
    </div>
    
    {{-- Tabel Perusahaan --}}
    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
        <a href="/table/siswa" class="btn btn-primary mb-2"><span class="bi bi-info"> Detail</span></a>
        <table class="table table-responsive-sm">
            <thead class="thead text-white" style="background-color: #007bff">
                <tr>
                    <th scope="col">Nama Perusahaan</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Jurusan</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($perusahaan as $perusahaan)
                  <tr>
                        <td>{{ $perusahaan->nama_perusahaan }}</td>
                        <td>{{ $perusahaan->alamat_perusahaan }}</td>
                        <td>{{ $perusahaan->kejuruan }}</td>
                  </tr>
              @endforeach
            </tbody>
          </table>
    </div>
</div>
{{-- If Kembali --}}
@if (Auth()->user()->level == 'siswa')
    <a href="/dashboard/siswa" class="btn btn-danger bi bi-arrow-left btn-sm"> Kembali</a>
@endif
@if (Auth()->user()->level == 'pembimbing')
    <a href="/dashboard/pembimbing" class="btn btn-danger bi bi-arrow-left btn-sm"> Kembali</a>
@endif
@if (Auth()->user()->level == 'admin')
    <a href="/adminHome" class="btn btn-danger bi bi-arrow-left btn-sm"> Kembali</a>
@endif

@endsection