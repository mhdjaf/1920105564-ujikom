@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="container mb-5">
    <form action="/editSiswa" method="post">
        @csrf
        <div class="card mt-3">
        <div class="card-body">
         <h3>Profile</h3>
            <img width="150px"  src="\foto_users\{{ $data->foto_users }}">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
              Ubah Foto
            </button>
            <input type="hidden" name="id" value="{{ $data->id }}">
            <div class="mb-3">
              <label for="" class="form-label">Nama Lengkap</label>
              <input type="text" class="form-control" name="name" id="" value="{{ $data->name }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Jenis Kelamin</label>
              <select class="form-control" name="jk" id="" style="height: 40px">
                <option selected value="{{ $data->jk }} ">{{ $data->jk }}</option>
                 <option>Laki-laki</option>
                 <option>Perempuan</option>
              </select>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Kelas</label>
              <input type="text" class="form-control" name="kelas" id="" value="{{ $data->kelas }}">
            </div>
            <div class="mb-3">
              @if ($data->level == 'pembimbing')
                <label for="" class="form-label">NIP</label>
              @else
                <label for="" class="form-label">NIS</label>
              @endif
              <input type="text" class="form-control" name="nis" id="" value="{{ $data->nis }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Kejuruan</label>
              <select class="form-control" name="kejuruan_id" id="" style="height: 40px">
                  @if ($data->kejuruan_id == null)
                  @else
                    <option selected value="{{ $data->jurusan->jurusan }} ">{{ $data->jurusan->jurusan }}</option>
                  @endif
                  @foreach ($data3 as $item)
                    <option value="{{ $item->id }}">{{ $item->jurusan }}</option>
                  @endforeach
              </select>
            </div>
            @if ($data->level == 'siswa')
            <div class="mb-3">
              <label for="" class="form-label">Pembimbing</label>
              <select class="form-control" name="pembimbing_id" id="" style="height: 40px">
                @if ($data->pembimbing_id == null)
                    <option value="">Siswa Belum memiliki pembimbing</option>
                @else
                  <option selected value="{{ $data->pembimbing->id }}">{{ $data->pembimbing->name }}</option>
                @endif
                  @foreach ($data2 as $i)
                  <option value="{{ $i->id }}">{{  $i->name}}</option>
                  @endforeach 
              </select>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Tempat PRAKERIN</label>
              <select class="form-control" name="perusahaan_id" id="" style="height: 40px">
                @if ($data->perusahaan_id == null)
                <option selected value="">Siswa belum memilih tempat PRAKERIN</option>
                @else
                  <option selected value="{{ $data->perusahaan->id }}">{{ $data->perusahaan->nama_perusahaan }}</option>
                @endif
                @foreach ($data1 as $i)
                <option value="{{ $i->id }}">{{  $i->nama_perusahaan}}</option>
                @endforeach 
              </select>
            </div>
            @endif
        </div>
        </div>
        <div class="card my-3">
          <div class="card-body">
            <h3>User Information</h3>
            <div class="mb-3">
              <label for="" class="form-label">Username</label>
              <input type="text" class="form-control" name="username" value="{{ $data->username }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">email</label>
              <input type="email" class="form-control" name="email"  value="{{ $data->email }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">No Telepon</label>
              <input type="number" class="form-control" name="no_telp"  value="{{ $data->no_telp }}">
            </div>
        </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>

        <a href="/detail/{{ $data->id }}" class="btn btn-danger bi bi-arrow-left"> Kembali</a> 
    </form>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Foto</h5>
        
      </div>
      <div class="modal-body">
        <form action="/updateFotoProfile" method="post" enctype="multipart/form-data">
            @csrf
            <label for="">Upload Foto Baru</label><br>
            <input type="hidden" name="id" value="{{ $data->id }}">
            <input type="file" name="foto" id="">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" value="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
    </div>
  </div>
</div>
@endsection