@extends('layouts.siswa.dashboard')

@section('body')
@foreach ($data as $item)
    
@if ($item)
  <div class="alert alert-primary alert-dismissible fade show mt-2" role="alert">
    {{ $item->kegiatan }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
@endforeach
<div class="row">

  <div class="col-lg-5 d-flex justify-content-center">
    <img src="/img/2.png" alt="" id="image" style="width: 490px">
  </div>

  <div class="col-lg-7 mt-4">

    <div class="ml-5 mt-3">
      <span class="text-uppercase" id="title" style="font-weight: 500">menu</span>
    </div>

    <div class="row" id="menu" style="margin-left: 40px; margin-top: 50px">

      <div class="col-xs-7" id="Jurnal">
      @if (Auth()->User()->pembimbing_id == null) 
        <a href="/home"><img src="/img/jurnal.png" alt="jurnal" style="width: 70px; margin-right: 100px; opacity: 0.5" onclick="alert('Kamu belum bisa mengisi Jurnal harian')" ></a>
      @else
        <a href="/jurnal"><img src="/img/jurnal.png" alt="jurnal" style="width: 70px; margin-right: 100px; "></a>
      @endif
        <p id="labelPilihJurusan" class="mb-3" style="margin-left: 17px; margin-top:-25px;  "><br>Jurnal<br>Harian</p>
      </div>
      <div class="col-xs-7 text-end" id="pengajuan">
        <a href="/dashboard/siswa/pengajuan_pkl"><img src="/img/pengajuan.png" alt="jurnal" style="width: 70px; margin-right: 100px;"></a>
        <p id="labelPengajuanPKL"style="margin-left: 17px; margin-top:-25px"><br>Pengajuan<br>PKL</p>
      </div>

      <div class="col-xs-4" id="informasi">
        <a href="/table"><img src="/img/tabel.png" alt="jurnal" style="width: 70px; margin-right: 100px"></a>
        <p id="labelInformasi" style="margin-left: 17px; margin-top:-25px"><br>Tabel</p>
      </div>

      <div class="col-xs-4" id="informasi">
        @if (Auth()->User()->pembimbing_id == null)
          <a href="/home"><img src="/img/typing.png" alt="jurnal" style="width: 70px; margin-right: 100px; margin-top: 15px;  opacity: 0.5" onclick="alert('Kamu belum bisa menyusun laporan')"></a>
        @else
          <a href="/laporan"><img src="/img/typing.png" alt="jurnal" style="width: 70px; margin-right: 100px; margin-top: 15px; "></a>
        @endif
        <p id="labellaporan" style="margin-left: 17px; margin-top:-25px"><br>Penyusunan<br>Laporan</p>
      </div>
    </div>

  </div>
</div>

@endsection