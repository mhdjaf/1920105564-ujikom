@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="row mt-4">
    <div class="col">
        <table class="table">
            <thead>
                <tr>
                    <th>NIS</th>
                    <th>Nama Siswa</th>
                    <th>File Laporan PRAKERIN</th>
                    <th>Keterangan</th>
                    <th>Dibauat</th>
                    <th>Diupdate</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $item)
                @if ($item->status != 'Laporan sudah di tanda tangan')
                    
                <tr>
                    <td scope="row">{{ $item->nis }}</td>
                    <td scope="row">{{ $item->name }}</td>
                    <td scope="row"><a href="/download/{{ $item->laporan->file_laporan }}" class="btn btn-primary">Download</a> </td>
                    <td scope="row">
                        @if ($item->laporan->keterangan == 'Laporan siap di cetak')
                            <div class="alert alert-primary" role="alert">{{ $item->laporan->keterangan }}</div>
                        @elseif ($item->laporan->keterangan == 'Laporan sudah di tanda tangan')
                            <div class="alert alert-success" role="alert">{{ $item->laporan->keterangan }}</div>
                        @else
                            <div class="alert alert-warning" role="alert">{{ $item->laporan->keterangan }}</div>
                        @endif
                    </td>
                    <td scope="row">{{ $item->laporan->created_at }}</td>
                    <td scope="row">{{ $item->laporan->updated_at }}</td>
                    <td scope="row">
                        @if ($item->laporan->keterangan != 'Laporan sudah di tanda tangan')
                            <a href="/validateLaporan/{{ $item->id }}" class="btn btn-primary">Validasi</a>
                        @endif
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
        <a href="/home" class="btn btn-danger btn-sm bi bi-arrow-left"> Kembali</a>
    </div>
</div>


@endsection