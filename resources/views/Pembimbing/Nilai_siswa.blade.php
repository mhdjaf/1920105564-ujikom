@extends('layouts.pembimbing.dashboard')

@section('body')

    <div class="container my-3">
        <div class="row ">
            <table class="table table-striped table-responsive text-center">
                <thead class="thead-inverse">
                    <tr>
                        <th class="text-center">NIS</th>
                        <th class="text-center">Nama Lengkap</th>
                        <th class="text-center">Kehadiran</th>
                        <th class="text-center">Tanggung Jawab</th>
                        <th class="text-center">Kedisiplinan</th>
                        <th class="text-center">Pekerjaan</th>
                        <th class="text-center">Tanggal Dibuat</th>
                        <th class="text-center">aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        @if ($item->status == "Laporan Siap")
                            @if ($item->nilai_id == null)
                                <tr>
                                    <td scope="row">{{ $item->nis }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>Null</td>
                                    <td>Null</td>
                                    <td>Null</td>
                                    <td>Null</td>
                                    <td>Null</td>
                                    <td><a href="/createNilai/{{ $item->id }}" class="btn btn-primary btn-sm">Tambah Nilai</a></td>
                                </tr>
                            @else
                                <tr>
                                    <td scope="row">{{ $item->nis }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->nilai->kehadiran }}</td>
                                    <td>{{ $item->nilai->tanggung_jawab }}</td>
                                    <td>{{ $item->nilai->kedisiplinan }}</td>
                                    <td>{{ $item->nilai->pekerjaan }}</td>
                                    <td>{{ $item->nilai->created_at }}</td>
                                    <td><a href="/updateNilai/{{ $item->id }}" class="btn btn-primary btn-sm">Edit</a></td>
                                </tr>
                            @endif
                        @elseif ($item->status == "Nilai Sudah di input")
                            @if ($item->nilai_id == null)
                                <tr>
                                    <td scope="row">{{ $item->nis }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>Null</td>
                                    <td>Null</td>
                                    <td>Null</td>
                                    <td>Null</td>
                                    <td>Null</td>
                                    <td><a href="/createNilai/{{ $item->id }}" class="btn btn-primary btn-sm">Tambah Nilai</a></td>
                                </tr>
                            @else
                                <tr>
                                    <td scope="row">{{ $item->nis }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->nilai->kehadiran }}</td>
                                    <td>{{ $item->nilai->tanggung_jawab }}</td>
                                    <td>{{ $item->nilai->kedisiplinan }}</td>
                                    <td>{{ $item->nilai->pekerjaan }}</td>
                                    <td>{{ $item->nilai->created_at }}</td>
                                    <td><a href="/updateNilai/{{ $item->id }}" class="btn btn-primary btn-sm">Edit</a></td>
                                </tr>
                            @endif
                        @endif
                    @endforeach
                </tbody>
            </table>
            <a href="/dashboard/pembimbing" class="btn btn-danger btn-sm bi bi-arrow-left"> Kembali</a>
        </div>
    </div>
@endsection