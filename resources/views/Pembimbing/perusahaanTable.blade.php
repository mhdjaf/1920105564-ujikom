@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="container my-3">
    <div class="card">
        <div class="card-body">
            @if (Auth()->user()->level == 'admin')
                <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="bi bi-plus"></i> Perusahaan</button>
            @endif
            <table class="table table-striped table-responsive-xl">
                <thead class="thead-inverse">
                    <tr>
                        <th>Id</th>
                        <th>Nama Perusahaan</th>
                        <th>Alamat Perusahaan</th>
                        <th>Menerima Jurusan</th>
                        <th>Bidang Kerjasama</th>
                        @if (Auth()->User()->level == 'admin')
                        <th>Aksi</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td scope="row">{{ $item->id }}</td>
                                <td>{{ $item->nama_perusahaan }}</td>
                                <td>{{ $item->alamat_perusahaan }}</td>
                                <td>{{ $item->kejuruan }}</td>
                                <td>{{ $item->kerjasama }}</td>
                                @if (Auth()->User()->level == 'admin')
                                    <td>
                                        <a href="/updatePerusahaan/{{ $item->id }}" class="btn btn-warning btn-sm bi bi-pencil text-white"></a>
                                        <a href="/deletePerusahaan/{{ $item->id }}" class="btn btn-danger btn-danger btn-sm bi bi-trash"></a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
            </table>
            <a href="/table" class="bi bi-arrow-left btn btn-danger btn-sm"> Back</a>
            
        </div>
    </div>
</div>

{{-- Modal --}}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Company</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/createPerusahaan" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nama Perusahaan:</label>
                        <input type="text" class="form-control" id="recipient-name" name="nama_perusahaan">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Alamat Perusahaan:</label>
                        <input type="text" class="form-control" id="recipient-name" name="alamat_perusahaan">
                    </div>
                    <div class="form-group">
                        <label for="">Kerjasama dengan Keahlian : </label><br>
                        @foreach ($jurusan as $item)
                        <input type="checkbox" name="jurusan[]" value="{{ $item->jurusan }}">{{ $item->jurusan }}<br>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label for="">Kerjasama Di bidang: </label><br>
                        <input type="checkbox" name="kerjasama[]" value="Rekruitmen">Rekruitmen<br>
                        <input type="checkbox" name="kerjasama[]" value="Prakerin">Prakerin<br>
                        <input type="checkbox" name="kerjasama[]" value="Penyelarasan Kurikulum">Penyelarasan Kurikulum<br>
                        <input type="checkbox" name="kerjasama[]" value="Kunjungan Industri">Kunjungan Industri<br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection