@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="container">
    <div class="card">
        <div class="card-body">
            <form action="/updateNilai" method="POST">
                @csrf
                @foreach ($data as $item)
                <input type="hidden" name="id" value="{{ $item->nilai->id }}">
                <div class="mb-3">
                    <label for="" class="form-label">NIS</label>
                    <input type="text" class="form-control" name="" id="" aria-describedby="helpId" value="{{ $item->nis }}" readonly>
                </div>
                <div class="mb-3">
                    <label for="" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="" id="" aria-describedby="helpId" value="{{ $item->name }}" readonly>
                </div>
                <div class="mb-3">
                  <label for="" class="form-label">Nilai Kehadiran</label>
                  <select class="form-control" name="kehadiran" id="">
                    <option value="{{ $item->nilai->kehadiran }}" selected>{{ $item->nilai->kehadiran }}</option>
                    <option value="A">A</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B">B</option>
                    <option value="B-">B-</option>
                    <option value="C+">C+</option>
                    <option value="C">C</option>
                    <option value="C-">C-</option>
                    <option value="D+">D+</option>
                    <option value="D">D</option>
                    <option value="D-">D-</option>
                    <option value="E">E</option>
                  </select>
                </div>
                <div class="mb-3">
                  <label for="" class="form-label">Nilai Tanggung Jawab</label>
                  <select class="form-control" name="tanggung_jawab" id="">
                    <option value="{{ $item->nilai->tanggung_jawab }}" selected>{{ $item->nilai->tanggung_jawab }}</option>
                    <option value="A">A</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B">B</option>
                    <option value="B-">B-</option>
                    <option value="C+">C+</option>
                    <option value="C">C</option>
                    <option value="C-">C-</option>
                    <option value="D+">D+</option>
                    <option value="D">D</option>
                    <option value="D-">D-</option>
                    <option value="E">E</option>
                  </select>
                </div>
                <div class="mb-3">
                  <label for="" class="form-label">Nilai Kedisiplinan</label>
                  <select class="form-control" name="kedisiplinan" id="">
                    <option value="{{ $item->nilai->kedisiplinan }}" selected>{{ $item->nilai->kedisiplinan }}</option>
                    <option value="A">A</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B">B</option>
                    <option value="B-">B-</option>
                    <option value="C+">C+</option>
                    <option value="C">C</option>
                    <option value="C-">C-</option>
                    <option value="D+">D+</option>
                    <option value="D">D</option>
                    <option value="D-">D-</option>
                    <option value="E">E</option>
                  </select>
                </div>
                <div class="mb-3">
                  <label for="" class="form-label">Nilai Pekerjaan</label>
                  <select class="form-control" name="pekerjaan" id="">
                    <option value="{{ $item->nilai->pekerjaan }}" selected>{{ $item->nilai->pekerjaan }}</option>
                    <option value="A">A</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B">B</option>
                    <option value="B-">B-</option>
                    <option value="C+">C+</option>
                    <option value="C">C</option>
                    <option value="C-">C-</option>
                    <option value="D+">D+</option>
                    <option value="D">D</option>
                    <option value="D-">D-</option>
                    <option value="E">E</option>
                  </select>
                </div>
                @endforeach
                <button type="submit" class="btn btn-primary|secondary|success|danger|warning|info|light|dark|link">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection