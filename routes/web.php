<?php

use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiswaController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Auth::routes();

Route::get('/login/admin', [App\Http\Controllers\HomeController::class, 'login4admin'])->middleware('guest');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('ceklevel:siswa,admin,pembimbing');
Route::get('/adminHome', [App\Http\Controllers\AdminController::class, 'index'])->name('homeAdmin')->middleware('ceklevel:admin');
Route::post('/kodeakses', [App\Http\Controllers\HomeController::class, 'kodeakses']);

Route::get('/dashboard/siswa', [App\Http\Controllers\SiswaController::class, 'dashboard'])->middleware('ceklevel:siswa', 'auth');
Route::get('/dashboard/pembimbing', [App\Http\Controllers\PembimbingController::class, 'dashboard'])->middleware('ceklevel:pembimbing', 'auth');
Route::get('/dashboard/siswa/pengajuan_pkl', [App\Http\Controllers\SiswaController::class, 'pengajuan_pkl'])->middleware('ceklevel:siswa', 'auth');
Route::get('/dashboard/monitoring', [App\Http\Controllers\PembimbingController::class, 'monitoring'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/dashboard/jurnalSiswa', [App\Http\Controllers\PembimbingController::class, 'jurnalSiswa'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/dashboard/Cek_kegiatan/{id}', [App\Http\Controllers\PembimbingController::class, 'cekKegiatan'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/dashboard/Pengajuan_PKL', [App\Http\Controllers\PembimbingController::class, 'Pengajuan_PKL'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/dashboard/pengajuanSidang', [App\Http\Controllers\PembimbingController::class, 'Pengajuan_Sidang'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/dashboard/nilai', [App\Http\Controllers\PembimbingController::class, 'nilai'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/createNilai/{id}', [App\Http\Controllers\PembimbingController::class, 'CreateNilai'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/updateNilai/{id}', [App\Http\Controllers\PembimbingController::class, 'editNilai'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/createNilai', [App\Http\Controllers\PembimbingController::class, 'postNilai'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/updateNilai', [App\Http\Controllers\PembimbingController::class, 'updateNilai'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/updateKeteranganJurnal', [App\Http\Controllers\PembimbingController::class, 'updateKeteranganJurnal'])->middleware('ceklevel:admin,pembimbing', 'auth');

Route::get('/editProfile', [App\Http\Controllers\SiswaController::class, 'index'])->name('editProfile')->middleware('auth');
Route::post('/editProfile', [App\Http\Controllers\SiswaController::class, 'update'])->name('editProfile')->middleware('auth');
Route::post('/updateFotoProfile', [App\Http\Controllers\SiswaController::class, 'updateFoto'])->middleware('auth');

Route::get('/siswaTables', [App\Http\Controllers\SiswaController::class, 'showAllSiswa'])->name('showAllSiswa')->middleware('ceklevel:admin,pembimbing,siswa', 'auth');
Route::get('/detail/{id}', [App\Http\Controllers\SiswaController::class, 'detailSiswa'])->name('detailSiswa')->middleware('ceklevel:admin,pembimbing,siswa', 'auth');

Route::get('/laporan', [App\Http\Controllers\SiswaController::class, 'laporan'])->name('laporan')->middleware('auth');
Route::post('/createLaporan', [App\Http\Controllers\SiswaController::class, 'createLaporan'])->name('createLaporan')->middleware('auth');
Route::post('/updateLaporan', [App\Http\Controllers\SiswaController::class, 'updateLaporan'])->name('updateLaporan')->middleware('auth');
Route::post('/updateKetLaporan', [App\Http\Controllers\SiswaController::class, 'updateKetLaporan'])->name('updateLaporan')->middleware('auth');
Route::get('/validateLaporan/{id}', [App\Http\Controllers\PembimbingController::class, 'validateLaporan'])->name('validasiLaporan')->middleware('auth');
Route::post('/validasiLaporan', [App\Http\Controllers\PembimbingController::class, 'validasiLaporan'])->name('validasiLaporan')->middleware('auth');

Route::get('/pembimbingTables', [App\Http\Controllers\PembimbingController::class, 'showAllPembimbing'])->name('showAllPembimbing')->middleware('ceklevel:admin,pembimbing,siswa', 'auth');
Route::get('/createPembimbing', [App\Http\Controllers\PembimbingController::class, 'createPembimbing'])->name('createPembimbing')->middleware('ceklevel:admin', 'auth');
Route::post('/createPembimbing', [App\Http\Controllers\PembimbingController::class, 'insertPembimbing'])->name('createPembimbing');
Route::get('/detailPembimbing/{id}', [App\Http\Controllers\PembimbingController::class, 'detailPembimbing'])->middleware('ceklevel:admin,pembimbing,siswa', 'auth');

Route::get('/cekLaporan', [App\Http\Controllers\PembimbingController::class, 'cekLaporan'])->middleware('ceklevel:admin,pembimbing,siswa', 'auth');

Route::get('/editSiswa/{id}', [App\Http\Controllers\AdminController::class, 'formupdate'])->name('formeditProfile')->middleware('ceklevel:admin', 'auth');
Route::get('/editPembimbing/{nip}', [App\Http\Controllers\AdminController::class, 'formupdatePembimbing'])->middleware('ceklevel:admin', 'auth');
Route::post('/editSiswa', [App\Http\Controllers\AdminController::class, 'update'])->name('editSiswa');
Route::get('/setting', [App\Http\Controllers\AdminController::class, 'setting']);
Route::post('/insertJurusan', [App\Http\Controllers\AdminController::class, 'insertJurusan']);
Route::post('/updateJurusan', [App\Http\Controllers\AdminController::class, 'updateJurusan']);
Route::get('/deleteJurusan/{id}', [App\Http\Controllers\AdminController::class, 'deleteJurusan']);
Route::get('/deletePerusahaan/{id}', [App\Http\Controllers\AdminController::class, 'deletePerusahaan']);
Route::get('/updatePerusahaan/{id}', [App\Http\Controllers\AdminController::class, 'updatePerusahaan']);
Route::post('/updatePerusahaan', [App\Http\Controllers\AdminController::class, 'editPerusahaan']);

Route::post('/createAccount', [App\Http\Controllers\AdminController::class, 'insert'])->middleware('ceklevel:admin', 'auth');
Route::get('/deleteSiswa/{id}', [App\Http\Controllers\AdminController::class, 'delete'])->middleware('ceklevel:admin', 'auth');
Route::get('/deletePembimbing/{nip}/{id}', [App\Http\Controllers\AdminController::class, 'deletePembimbing'])->middleware('ceklevel:admin', 'auth');

Route::get('/lanjut', [App\Http\Controllers\SiswaController::class, 'nextProgress'])->middleware('auth');
Route::post('/upload/pengantar', [App\Http\Controllers\SiswaController::class, 'upload_pengantar'])->middleware('auth');
Route::get('/download/{file_name}', [App\Http\Controllers\SiswaController::class, 'download'])->middleware('auth');

Route::post('/download', [App\Http\Controllers\PembimbingController::class, 'download'])->middleware('auth');
Route::get('/validasiPengantar/{id}', [App\Http\Controllers\PembimbingController::class, 'validasiPengantar'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/konfirmasiPengajuan/{id}', [App\Http\Controllers\PembimbingController::class, 'konfirmasiPengajuan'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/prosesValidasiPengantar', [App\Http\Controllers\PembimbingController::class, 'prosesValidasiPengantar'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/addPembimbing', [App\Http\Controllers\PembimbingController::class, 'addPembimbing'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/konfirmasiPengajuan', [App\Http\Controllers\PembimbingController::class, 'proseskonfirmasiPengajuan'])->middleware('auth');

Route::post('/createPerusahaan', [App\Http\Controllers\AdminController::class, 'insertPerusahaan'])->middleware('ceklevel:admin', 'auth');
Route::post('/insertTempatPkl', [App\Http\Controllers\SiswaController::class, 'insertTempatPkl'])->middleware('auth');

Route::post('/gantiStatus', [App\Http\Controllers\SiswaController::class, 'gantiStatus']);

Route::get('/jurnal', [App\Http\Controllers\JurnalController::class, 'index'])->middleware('auth');
Route::post('/jurnal/tambah', [App\Http\Controllers\JurnalController::class, 'tambah'])->middleware('auth');
Route::get('/jurnal/edit/{id}', [App\Http\Controllers\JurnalController::class, 'edit'])->middleware('auth');
Route::post('/jurnal/update', [App\Http\Controllers\JurnalController::class, 'update'])->middleware('auth');
Route::get('/jurnal/delete/{id}', [App\Http\Controllers\JurnalController::class, 'delete'])->middleware('auth');

Route::get('/gantiPassword', [App\Http\Controllers\SiswaController::class, 'gantiPassword'])->middleware('auth');
Route::patch('/gantiPassword', [App\Http\Controllers\SiswaController::class, 'updatePassword'])->name('user.password.update')->middleware('auth');

Route::get('/table', [App\Http\Controllers\HomeController::class, 'ShowTables'])->middleware('auth');
Route::get('/table/siswa', [App\Http\Controllers\HomeController::class, 'table_siswa'])->middleware('auth');
Route::get('/table/siswa/cari', [App\Http\Controllers\HomeController::class, 'siswa_search'])->middleware('auth');
Route::get('/table/pembimbing', [App\Http\Controllers\HomeController::class, 'table_pembimbing'])->middleware('auth');
Route::get('/table/perusahaan', [App\Http\Controllers\HomeController::class, 'table_perusahaan'])->middleware('auth');
Route::get('/profile', [App\Http\Controllers\HomeController::class, 'profile'])->middleware('auth');
