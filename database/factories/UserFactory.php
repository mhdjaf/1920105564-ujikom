<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'jk' => 'Laki laki',
            'email' => $this->faker->unique()->safeEmail(),
            'kelas' => 'XII',
            'no_telp' => '08979240794',
            'jurusan_id' => $this->faker->numberBetween($int1 = 1, $int2 = 2),
            'nis' => $this->faker->unique()->randomNumber(),
            'username' => $this->faker->unique()->username(),
            'level' => 'siswa',
            'email_verified_at' => now(),
            'password' => bcrypt('password'),
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
